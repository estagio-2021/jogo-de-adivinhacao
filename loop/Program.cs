﻿using System;

namespace loop
{
    class Program
    {
        static void Main(string[] args)
        {
            int pensamento;
            int chute;
            int tentativas;
            pensamento = Convert.ToInt16(Console.ReadLine());
            tentativas = 0;
            
            do
            {
                chute = Convert.ToInt16(Console.ReadLine());
                tentativas++;
            } while (chute != pensamento);
            
            // while (chute != pensamento)
            // {
            //     chute++;
            //     tentativas++;
            // }
            
            Console.WriteLine("Tentativas: ");
            Console.WriteLine(tentativas);
            Console.WriteLine("Chute: ");
            Console.WriteLine(chute);
        }
    }
}